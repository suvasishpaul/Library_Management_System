<?php

namespace App\Library;

use App\Message\Message;
use App\Utility\Utility;

class Library{
    public $id = "";
    public $student_id = "";
    public $book_id="";
    public $searchByCategory = "";
    public $search = "";

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "lms");
    }

    public function prepare($data)
    {
        if (array_key_exists('title', $data))
            $this->title = $data['title'];
        if (array_key_exists('id', $data))
            $this->id = $data['id'];
        if (array_key_exists('student_id', $data))
            $this->student_id = $data['student_id'];
        if (array_key_exists('book_id', $data))
            $this->book_id = $data['book_id'];
        if (array_key_exists("description", $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists("category", $data)) {
            $this->searchByCategory = $data['category'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }
       return $this;
    }


    public function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->searchByCategory)){
            $whereClause.=" AND  category LIKE '%".$this->searchByCategory."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  name LIKE '%".$this->search."%' OR author LIKE '%".$this->search."%'";
        }


        $_allbook = array();
        $query = "SELECT * FROM `booklist` WHERE ".$whereClause;
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_object($result)) {
            $_allbook[] = $row;
        }
        return $_allbook;
    }


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM `booklist` LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `lms`.`booklist`";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }
    
    
    public function request(){
        $query = "INSERT INTO `requested_books` (`book_id`, `student_id`) VALUES ('".$this->book_id."', '".$this->student_id."')";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Request has been sent successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function requestedBooks(){
        $allInfo = array();
        $query = "SELECT * FROM booklist RIGHT JOIN requested_books ON `booklist`.`id`=`requested_books`.`book_id` ORDER BY `booklist`.`id`";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $allInfo[]=$row;
        return $allInfo;
    }


    public function approval(){
        $query = "UPDATE `booklist` SET `amount` = `amount`-1 WHERE `booklist`.`id` = ".$this->book_id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("../views/requested_books.php");
        }
        else
            echo "ERROR";
    }


    public function sotreBorrow(){
        $date=date("Y-m-d");
        date_add($date,date_interval_create_from_date_string("7 days"));
        $due_date = date_format($date,"Y-m-d");
        $query = "INSERT INTO `borrow` (`book_id`, `student_id`, `borrow_date`, `due_date`, `return_date`) VALUES ('".$this->book_id."', '".$this->student_id."', '".date("Y-m-d")."', '".$due_date."', '')";
        $result = mysqli_query($this->conn, $query);
        if(!$result)
            echo "ERROR";
    }
}
//include_once ('../../views/requested_books.php')