<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->log_out();
if($status){
    Message::message("You are successfully logged-out");
    return Utility::redirect('admin_login.php');
}