<?php
include_once ('../../vendor/autoload.php');
use  App\Library\Library;
$info =new Library();
$allInfo=$info->index();



?>
<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap Admin Theme v3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.php">Bootstrap Admin Theme</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form">
	                       <input type="text" class="form-control" placeholder="Search...">
	                       <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Search</button>
	                       </span>
	                  </div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="admin_logout.php">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>


					<li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i>  Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i>  Requsted Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i>  Return Book</a></li>

                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">


			  <div class="table-responsive">
				  <table class="table table-striped">
					  <thead>
					  <tr>
						  <th>#</th>
						  <th>Book Name</th>
						  <th>Author Name</th>
						  <th>Edition</th>
						  <th>Book Cover</th>
						  <th>Location</th>
						  <th>Availablity</th>

					  </tr>
					  </thead>
					  <tbody>
					  <?php $sl=0; foreach($allInfo as $info) { $sl++; ?>

					  <tr>
						  <td width="8%"> <?php echo $sl ?> </td>
						  <td width="8%"> <?php echo $info->name ?></td>
						  <td width="15%"> <?php echo $info->author ?> </td>
						  <td class="text-justify" width="10%"><?php echo $info->edition ?></td>
						  <td class="text-justify" width="15%"><img src="../../../images/page1_pic5.jpg" class="img-responsive" width="100px" height="100px"/></td>
						  <td class="text-justify" width="10%"><?php echo $info->location ?></td>
						  <?php if($info->amount>0) : ?>
						  <td width="15%"><a href="" class="btn btn-success leftpad5" role="button">Available<span class="badge"> <?php echo $info->amount ?> </span></a>
							  <?php endif ?>
							  <?php if($info->amount==0) : ?>
						  <td width="15%"><a href=""  class="btn btn-danger leftpad5" role="button">Not Available<span class="badge"> <?php echo $info->amount ?> </span></a>
							  <?php endif ?>
					  </tr>

					  <?php } ?>


					  </tbody>
				  </table>
				  --!>
			  </div>
			  <?php if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) { ?>
			  <ul class="pagination">
				  <?php if($pageNumber>1):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber-1 ?>'> Prev </a></li> <?php endif;?>
				  <?php echo $pagination; ?>
				  <?php if($pageNumber<$totalPage):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber+1 ?>'> Next </a></li> <?php endif;?>
			  </ul>
			  <?php } ?>

		  </div>


		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../Resources/bootstrap/js/custom.js"></script>
  </body>
</html>