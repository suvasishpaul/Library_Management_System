<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

//Utility::dd($_POST);
$auth= new Auth();
$status= $auth->prepare($_POST)->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
                            <strong>Taken!</strong> Email has already been taken .
                </div>");
    return Utility::redirect('../../create_book.php');
}else{
    $obj= new User();
    $obj->prepare($_POST)->store();
}
