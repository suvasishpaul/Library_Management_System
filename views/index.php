<?php

session_start();

include_once('../vendor/autoload.php');

use App\Library\Library;
use App\User\Auth;

$auth = new Auth();
$logged_in = FALSE;
if ($auth->logged_in())
    $logged_in = TRUE;

if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='create_book.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category']))  {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}

//$availableTitle=$obj->getAllTitle();
//$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>

<!DOCTYPE html>
<html>
<head>
    <style>
        .leftpad5 {
            padding-right: 5px;
        }
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../Resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="../Resources/bootstrap/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script src="../Resources/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<div class="container">
    <div class="headingtop">
        <h2 class="headingwrite">Basis Library Management System</h2>
    </div>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../../../index.php">Back to Home</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="index.php?category=<?php echo "Science Fiction" ?>" >Science Fiction</a></li>
                <li><a href="index.php?category=<?php echo "Romantic Novel" ?>" >Romantic Novel</a></li>
                <?php if(!$logged_in) : ?>
                    <li><a href="user_login_signup.php">Log In</a></li>
                <?php endif ?>
                <?php if($logged_in) : ?>
                    <li><a href="Authentication/user_logout.php">Log Out</a></li>
                <?php endif ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form role="form">
                        <div class="form-group form-inline margintop">
                            <label class="lablewidth" for="sel1">Item per page</label>
                            <select class="form-control formwidth" id="sel1" name="itemPerPage">
                                <option>5</option>
                                <option>10</option>
                                <option selected>15</option>
                                <option>20</option>
                                <option>25</option>
                            </select>
                            <button class=" btn btn-default buttonwidth" type="submit">Go!</button>

                        </div>
                    </form>
                </li>

            </ul>
        </div>
    </nav>


    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li>
                    <form class="form-inline" action="index.php" method="get">
                        <input width="34%" placeholder="Search..." type="text" name="search">
                        <button class="btn btn-info" type="submit">Search</button>
                    </form>

                </li>

            </ul>
        </div>
    </nav>


</div>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Book Name</th>
            <th>Author Name</th>
            <th>Edition</th>
            <th>Book Cover</th>
            <th>Location</th>
            <th>Availablity</th>

        </tr>
        </thead>
        <tbody>
        <?php $sl = 0;
        foreach ($allInfo as $info) {
            $sl++; ?>

            <tr>
                <td width="5%"> <?php echo $sl ?> </td>
                <td width="5%"> <?php echo $info->name ?></td>
                <td width="15%"> <?php echo $info->author ?> </td>
                <td class="text-justify" width="10%"><?php echo $info->edition ?></td>
                <td class="text-justify" width="20%"><img src="Resources/images/page1_pic5.jpg" class="img-responsive"
                                                          width="100px" height="100px"/></td>
                <td class="text-justify" width="10%"><?php echo $info->location ?></td>
                <?php if ($info->amount > 0) : ?>
                <td width="15%"><a href="" class="btn btn-success leftpad5" role="button">Available</a>
                    <?php endif ?>
                    <?php if ($info->amount == 0) : ?>
                <td width="15%"><a href="" class="btn btn-danger leftpad5" role="button">Not Available<span
                            class="badge"> </span></a>
                    <?php endif ?>
                <?php if($logged_in) : ?>
                <td><a href="book_request.php?book_id=<?php echo $info->id ?>" <?php if($info->amount==0) : ?> disabled="" <?php endif; ?> class="btn btn-primary">Borrow</a></td>
                <?php endif ?>
            </tr>

        <?php } ?>


        </tbody>
    </table>
</div>
<?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) { ?>
    <ul class="pagination">
        <?php if ($pageNumber > 1): ?>
            <li><a href='create_book.php?pageNumber=<?php echo $pageNumber - 1 ?>'> Prev </a></li> <?php endif; ?>
        <?php echo $pagination; ?>
        <?php if ($pageNumber < $totalPage): ?>
            <li><a href='create_book.php?pageNumber=<?php echo $pageNumber + 1 ?>'> Next </a></li> <?php endif; ?>
    </ul>
<?php } ?>

</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
<script>
    $(function () {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $("#title").autocomplete({
            source: availableTags
        });
    });
</script>

</body>
</html>
